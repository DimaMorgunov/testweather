import React from 'react';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import SearchComponent from './Component/SearchComponent'
import MapComponent from './Component/MapComponent'


const TabNavigator = createBottomTabNavigator({
    Map: MapComponent,
    Search: SearchComponent,
},
    {
        tabBarOptions: {
            upperCaseLabel: true,
            showLabel: true,
            lazyLoad: true,
            activeBackgroundColor: '#d1d2ee',
            inactiveBackgroundColor: '#0e95ee',
            labelStyle: {
                fontSize: 18,
                fontWeight: 'bold',
                color: '#fff',
                position: 'relative',
                alignSelf: 'center',
                marginBottom: 10,
            },
            tabStyle: {
                borderRadius: 20,
                marginHorizontal: 10,
            },

        },
    });

export default createAppContainer(TabNavigator);