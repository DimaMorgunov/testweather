import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import TabNavigator from './router'
import moment from 'moment';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component {
    state = {
        name: "",
        location: {
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },
        weather: null,
        currentDayWeather: {},
        weatherWeekly: [],
        pin: []
    };


    componentDidMount() {
        this.getLocation()
    }

    removePin = () => {
        this.setState({name: '', weatherWeekly: [], pin: []})
    };

    getLocation() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                const currentPosition = {};
                currentPosition.latitude = position.coords.latitude;
                currentPosition.longitude = position.coords.longitude;
                this.setState({location: {...this.state.location, ...currentPosition}});
            },
            (error) => this.setState({forecast: error.message}),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
        );
    }

    getWeather = async (coordinate) => {
        console.log('weather', coordinate)
        this.setState({location: {...this.state.location, ...coordinate}, pin: [coordinate]});
        let url = 'https://api.openweathermap.org/data/2.5/forecast?lat=' + coordinate.latitude + '&lon=' + coordinate.longitude + '&units=metric&appid=45163589862b0720cddcac3f772175c4';
        await fetch(url)
            .then(response => response.json())
            .then(data => {
                this.sortWeatherByDay(data)
                console.log(data)
            })
    };

    sortWeatherByDay = (data) => {
        const name = `${data.city.country}, ${data.city.name}`
        console.log(name, 'city name')
        let dayList = [];
        let weatherList = [];
        data.list.forEach(item => {
            const weatherObj = {};
            weatherObj.name = moment(item.dt_txt.split(" ")[0]).format('dddd');
            weatherObj.temp = item.main.temp
            weatherList.push({...weatherObj})
            dayList.push(moment(item.dt_txt.split(" ")[0]).format('dddd'))
        });
        var unique = weatherList.filter((item, index) =>  dayList.indexOf(item.name)== index);
        this.setState({currentDayWeather: weatherList[0], name: name, weatherWeekly: unique})

    };

    handleChangeState = (type, value) => {
        this.setState({...this.state, [type]: value})
    };

    render() {
        const {name, location, currentDayWeather, weatherWeekly, pin} = this.state;
        return (
            <TabNavigator
                screenProps={{
                    name: name,
                    location: location,
                    handleChangeState: this.handleChangeState,
                    getWeather: this.getWeather,
                    currentDayWeather: currentDayWeather,
                    weatherWeekly: weatherWeekly,
                    removePin: this.removePin,
                    pin: pin
                }}
            />
        );
    }
}

