import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import MapView, {Marker} from 'react-native-maps'

export default class MapComponent extends Component {

    onMapPress = (coordinate) => {
        console.log(this.props.screenProps)
        if (  this.props.screenProps.pin.length > 0) {
            this.props.screenProps.removePin();
        } else {
            /*this.props.screenProps.handleChangeState('locations', coordinate);*/
            this.props.screenProps.getWeather(coordinate)
        }
    };

    handleToSearch = () => {
        this.props.navigation.navigate('Search')
    };


    render() {
        const {name, currentDayWeather, pin} = this.props.screenProps;
        console.log(this.props.screenProps)
        return (
            <View style={{flex: 1}}>

                <MapView
                    onPress={(event) => this.onMapPress(event.nativeEvent.coordinate)}
                    style={styles.map}
                    followUserLocation={true}
                    initialRegion={this.props.screenProps.location}
                    region={this.props.screenProps.location}

                >
                    {/*<TouchableOpacity style={styles.headerTitleWrap}>
                        <Text style={styles.headerTitle}>
                            get location
                        </Text>
                    </TouchableOpacity>*/}

                    {pin.length && name.length ?
                        pin.map((item, index) =>
                            <Marker key={index} coordinate={item} onPress={(e) => {e.stopPropagation(); this.handleToSearch()}}>
                                    <View style={styles.markerWrap}>
                                        <View style={styles.rightArrowWrap}>
                                            <Text style={styles.markerText}>{name}</Text>
                                            <TouchableOpacity style={styles.arrowButton}>
                                                <Image source={require('../images/right-arrow.png')} style={styles.rightArrow}/>
                                            </TouchableOpacity>
                                        </View>
                                        <Text style={styles.markerText}>{currentDayWeather.temp > 0 ? `+${currentDayWeather.temp}°` : `${currentDayWeather.temp}°`}</Text>
                                    </View>
                                <Image source={require('../images/location-pin.png')} style={styles.markerImage}/>
                            </Marker>)
                        : null
                    }

                </MapView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    headerTitleWrap: {
        position: 'absolute',
        bottom: 50,
        backgroundColor: 'rgba(255, 255, 255, 1)',
    },
    headerTitle: {

        fontSize: 20,
        color: 'blue',
    },
    map: {
       flex:1
    },
    markerWrap: {
        width: 200,
        height: 80,
        backgroundColor: 'white',
        borderRadius: 10,
        zIndex: 10,
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    markerText: {
        fontSize: 16,
        color: 'grey'
    },
    markerImage: {
        width: 40,
        height: 40,
        marginLeft: 80
    },
    rightArrow: {
        width: 30,
        height: 30,
        marginLeft: 5
    },
    arrowButton: {
        width: 30,
        height: 30
    },
    rightArrowWrap: {
        flexDirection: 'row'
    }
});
