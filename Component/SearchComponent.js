
import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View, TouchableOpacity, Keyboard} from 'react-native';
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";

export default class SearchComponent extends Component {

    state = {
      showWeather: true
    };

    componentDidMount() {
        this.subs = [
            this.props.navigation.addListener('willFocus', this.componentWillFocus)
        ]
    }

    componentWillFocus = () => {
        console.log(this.input)
        this.keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            this._keyboardDidShow,
        );
        this.keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            this._keyboardDidHide,
        );
        console.log(this.props.screenProps.name)
        if(this.props.screenProps.name) {
            console.log(this.props.screenProps.name)
            this.input.refs.textInput.props.value = this.props.screenProps.name;
        }
    };

    _keyboardDidShow = () => {
      this.setState({showWeather: false})
    };

    _keyboardDidHide = () => {
        this.setState({showWeather: true})
    };

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    handleSearch = () => {
        console.log(this.input)
        this.input._onFocus()
    };

    render() {
        const { weatherWeekly } = this.props.screenProps;
        const { showWeather } = this.state;
        return (
            <View style={styles.container}>
                <GooglePlacesAutocomplete
                    placeholder='Search'
                    minLength={2}
                    autoFocus={false}
                    returnKeyType={'search'}
                    listViewDisplayed='auto'
                    fetchDetails={true}
                    renderDescription={row => row.description}
                    onPress={(data, details = null) => {
                        console.log(data, details);
                        const responseObj = {}
                        responseObj.latitude = details.geometry.location.lat;
                        responseObj.longitude = details.geometry.location.lng;
                        this.props.screenProps.getWeather({...responseObj});
                        this.props.screenProps.handleChangeState({'location': details.geometry.location})
                        this.props.navigation.navigate('Map')
                    }}

                    ref={ el => {this.input = el}}

                    getDefaultValue={() => this.props.screenProps.name}

                    query={{
                        key: 'AIzaSyAr_GwDEOX3STJkeuCWcwVjQ5yz3qNmo7E',
                        language: 'en',
                        types: '(cities)'
                    }}

                    styles={{
                        textInputContainer: {
                            width: '100%'
                        },
                        description: {
                            fontWeight: 'bold'
                        },
                        predefinedPlacesDescription: {
                            color: '#1faadb'
                        }
                    }}

                    nearbyPlacesAPI='GooglePlacesSearch'
                    GooglePlacesSearchQuery={{
                        rankby: 'distance',
                        types: 'food'
                    }}

                    filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']}

                    debounce={200}
                    renderRightButton={() => <TouchableOpacity style={styles.searchButton} onPress={() => this.handleSearch()}>
                                                <Text>Search</Text>
                                            </TouchableOpacity>
                    }
                />
                {
                    showWeather && weatherWeekly.length ?
                        weatherWeekly.map(item => (
                            <View style={styles.tempItemWrap}>
                                <Text style={styles.itemText}>{item.name}</Text>
                                <Text style={styles.itemText}>{item.temp > 0 ? `+${item.temp}°` : `${item.temp}°`}</Text>
                            </View>
                        ))
                        : null
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    searchButton: {
        alignSelf: 'center',
        alignItems: 'center',
        color: 'blue',
        marginHorizontal: 10
    },
    tempItemWrap: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '80%',
        height: 50,
        backgroundColor: '#a9b1e8',
        borderRadius: 10,
        marginBottom: 20,
        alignItems: 'center'
    },
    itemText: {
        fontSize: 20,
        color: '#ffffff'
    }

});
